﻿using CoreGraphics;
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using UniViewManager.Functionality;
using UniViewManager.Pagers;
using UniViewManager.UniPagerManager.Functionality;

namespace UniViewManager.Delegates
{
    public abstract class BaseTouchHandler : UIPanGestureRecognizer 
    {
        private BasePageController _controll = null;

        private readonly Action<UITouch, bool> _action = null;

        protected BaseTouchHandler(BasePageController controll)
        {
            _action += controll.TouchesReceived;

            this._controll = controll;
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);

            var touch = touches.AnyObject as UITouch;

            SendLocation(touch, false);
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            base.TouchesMoved(touches, evt);

            var touch = touches.AnyObject as UITouch;

            SendLocation(touch, false);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);

            var touch = touches.AnyObject as UITouch;

            SendLocation(touch, true);
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            var touch = touches.AnyObject as UITouch;

            SendLocation(touch, true);
        }

        public virtual void SendLocation(UITouch touch, bool ended)
        {
            _action(touch, ended);
        }
    }
}
