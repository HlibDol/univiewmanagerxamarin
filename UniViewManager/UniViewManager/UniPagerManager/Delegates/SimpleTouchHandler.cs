﻿using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using UniViewManager.Functionality;
using System;
using UniViewManager.Pagers;
using UniViewManager.UniPagerManager.Functionality;

namespace UniViewManager.Delegates
{
    public class SimpleTouchHandler : BaseTouchHandler
    {
        public SimpleTouchHandler(BasePageController controll, Pagers.UniPager pager) : base(controll)
        {

        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            base.TouchesMoved(touches, evt);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
        }        
    }
}
