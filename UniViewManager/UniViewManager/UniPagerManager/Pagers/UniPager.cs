﻿using CoreGraphics;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using UniViewManager.Delegates;
using UniViewManager.Functionality;
using UniViewManager.UniPagerManager.Functionality;

namespace UniViewManager.Pagers
{
    public abstract class UniPager : UIView
    {       
        /// <summary>
        ///  Use this in a stable and latest version
        /// </summary>
        //public delegate void MoveField();
        //public event MoveField ViewsMoved;

        protected List<UIView> _views = null;        

        protected BasePageController Controller = null;

        protected BaseTouchHandler TouchHandler = null;

        public UniPager(List<UIView> views, CGRect rect)
        {
            _views = views;

            Frame = rect;

            SetUpDefaultSettings();
        }

        public void SetNewView(UIView view)
        {
            _views.Add(view);
        }

        public void SetNewViews(List<UIView> views)
        {
            _views.AddRange(views);
        }

        private void SetUpDefaultSettings()
        {
            Controller = new PageControllerSimple(this, _views);

            TouchHandler = new SimpleTouchHandler(Controller, this)
            {
                MaximumNumberOfTouches = 1
            };

            AddGestureRecognizer(TouchHandler);
        }
    }
}
