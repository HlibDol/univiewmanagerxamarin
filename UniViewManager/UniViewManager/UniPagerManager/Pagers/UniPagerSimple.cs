﻿using System.Collections.Generic;
using UIKit;
using UniViewManager.Delegates;
using UniViewManager.Functionality;

namespace UniViewManager.Pagers
{
    public class UniPagerSimple : UniPager
    {
        public UniPagerSimple(List<UIView> views, CoreGraphics.CGRect rect) : base(views, rect)
        {

        }
    }
}
