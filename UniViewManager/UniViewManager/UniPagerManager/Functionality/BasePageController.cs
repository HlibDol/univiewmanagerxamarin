﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using UIKit;

namespace UniViewManager.UniPagerManager.Functionality
{
    public abstract class BasePageController : IControlable
    {
        private readonly Pagers.UniPager _pager;
        private readonly List<UIView> _viewsInside;

        protected int CurrentPageIndex;

        protected BasePageController(Pagers.UniPager pager, List<UIView> views)
        {
            _viewsInside = views;
            _pager = pager;

            LayoutSubViews();
        }

        public virtual void MoveViews(List<CGPoint> pointsToMove)
        {
            var rectChanger = _viewsInside[CurrentPageIndex].Frame;
            var difX = pointsToMove[0].X - pointsToMove[1].X;

            rectChanger.X += difX;

            if (((CurrentPageIndex == 0) && (difX > 0))
                || ((CurrentPageIndex == _viewsInside.Count - 1) && (difX < 0)))
                return;

            _viewsInside[CurrentPageIndex].Frame = rectChanger;

            LeftDirection(rectChanger, difX);

            RightDirection(rectChanger, difX);
        }

        private void RightDirection(CGRect rectChanger, nfloat difX)
        {
            if (CurrentPageIndex - 1 < 0) return;

            if (_viewsInside[CurrentPageIndex - 1].Superview == null)
            {
                _pager.AddSubview(_viewsInside[CurrentPageIndex - 1]);
            }

            rectChanger.X = _viewsInside[CurrentPageIndex - 1].Frame.X + difX;
            _viewsInside[CurrentPageIndex - 1].Frame = rectChanger;
        }

        private void LeftDirection(CGRect rectChanger, nfloat difX)
        {
            if (CurrentPageIndex + 1 >= _viewsInside.Count) return;

            if (_viewsInside[CurrentPageIndex + 1].Superview == null)
            {
                _pager.AddSubview(_viewsInside[CurrentPageIndex + 1]);
            }

            rectChanger.X = _viewsInside[CurrentPageIndex + 1].Frame.X + difX;
            _viewsInside[CurrentPageIndex + 1].Frame = rectChanger;
        }

        public void TouchesReceived(UITouch touch, bool ended)
        {
            var listOfPoints = new List<CGPoint>
            {
                touch.LocationInView(_pager),
                touch.PreviousLocationInView(_pager)
            };

            if (!ended)
                MoveViews(listOfPoints);
            else
                MoveEnded(listOfPoints[0]);
        }

        public virtual void SwitchViews(int receivedIndex)
        {
            CurrentPageIndex = receivedIndex;

            LayoutSubViews();
        }

        private void LayoutSubViews()
        {
            for (var j = 0; j < _viewsInside.Count; j++)
                if (j < CurrentPageIndex)
                {
                    _viewsInside[j].Frame = new CGRect(_pager.Frame.X - _pager.Frame.Width, _pager.Frame.Y,
                        _pager.Frame.Width, _pager.Frame.Height);

                   // _viewsInside[j].RemoveFromSuperview();
                       //_viewsInside[j].Hidden = true;
                }
                else if (j > CurrentPageIndex)
                {
                    _viewsInside[j].Frame = new CGRect(_pager.Frame.X + _pager.Frame.Width, _pager.Frame.Y,
                        _pager.Frame.Width, _pager.Frame.Height);

                    //_viewsInside[j].RemoveFromSuperview();
                    //_viewsInside[j].Hidden = true;
                }
                else
                {
                    _viewsInside[j].Frame = new CGRect(_pager.Frame.X , _pager.Frame.Y,
                       _pager.Frame.Width, _pager.Frame.Height);
                }
        }

        public virtual void MoveEnded(CGPoint lastTouch)
        {
            UIView viewToAnimate = null;
            var xOfSecondView = _pager.Frame.Width;

            var minDistance = _pager.Frame.Width / 2;

            for (var i = 0; i < _viewsInside.Count; i++)
            {
                if (_viewsInside[i].Frame.X >= _pager.Frame.Width/2 || _viewsInside[i].Frame.X <= -_pager.Frame.Width/2 
                    && Math.Abs(minDistance) > Math.Abs(_viewsInside[i].Frame.X))
                    continue;

                minDistance = _viewsInside[i].Frame.X;
                CurrentPageIndex = i;
                viewToAnimate = _viewsInside[CurrentPageIndex + 1];
            }

            _pager.BringSubviewToFront(_viewsInside[CurrentPageIndex]);

            var frame = _viewsInside[CurrentPageIndex].Frame;

            if (minDistance > 0)
            {
                xOfSecondView *= -1;
                viewToAnimate = _viewsInside[CurrentPageIndex - 1];
            }

            UIView.Animate(0.2f, delegate
                {
                    _viewsInside[CurrentPageIndex].Frame =
                        new CGRect(_pager.Frame.X, frame.Y, frame.Width, frame.Height);
                    viewToAnimate.Frame = new CGRect(_pager.Frame.X + xOfSecondView, frame.Y, frame.Width, frame.Height);
                });
            
            LayoutSubViews();
        }

        public virtual void ChangeCurrentView(int toIndex)
        {
            CurrentPageIndex = toIndex;
            LayoutSubViews();
        }

        public virtual void DeleteSubviews(int atIndex)
        {
            _viewsInside.RemoveAt(atIndex);
            LayoutSubViews();
        }

        public virtual void AddSubviews(UIView viewToAdd)
        {
            _viewsInside.Add(viewToAdd);
            LayoutSubViews();
        }
   }
}