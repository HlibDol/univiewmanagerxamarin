﻿using System.Collections.Generic;
using CoreGraphics;
using UIKit;

namespace UniViewManager.UniPagerManager.Functionality
{
    public interface IControlable
    {
        void MoveViews(List<CGPoint> pointsToMove);

        void TouchesReceived(UITouch touch, bool ended);

        void SwitchViews(int receivedIndex);
    }
}
