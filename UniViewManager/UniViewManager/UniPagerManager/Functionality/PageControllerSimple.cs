﻿using UniViewManager.Pagers;
using UniViewManager.Delegates;
using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;
using UniViewManager.UniPagerManager.Functionality;

namespace UniViewManager.Functionality
{
    public class PageControllerSimple : BasePageController
    {
        public PageControllerSimple(Pagers.UniPager pager, List<UIView> views) : base(pager, views)
        {
        }

        public override void MoveViews(List<CGPoint> pointsToMove)
        {
            base.MoveViews(pointsToMove);
        }
    }
}
