﻿using System;
using System.Collections.Generic;
using UIKit;
using UniViewManager.Pagers;

namespace UniViewManager
{
    internal class ViewController : UIViewController
    {
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            List<UIView> v = new List<UIView>();

            var random = new Random();
            random.Next(0, 255);

            for (var i = 0; i < 7; i++)
            {
                v.Add(new UIView() { BackgroundColor = UIColor.FromRGB((nfloat)random.Next(0, 255) / 255, (nfloat)random.Next(0, 255) / 255, (nfloat)random.Next(0, 255) / 255) });
            }

            View.AddSubview(new UniPagerSimple(v, View.Frame) { BackgroundColor = UIColor.Green });
        }
    }
}
